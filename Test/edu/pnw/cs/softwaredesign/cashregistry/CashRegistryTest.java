package edu.pnw.cs.softwaredesign.cashregistry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CashRegistryTest {

  @Test
  public void calculateTest() {
    double unitprice = 3;
    int quantity = 10;
    int discount = 20;
    CashRegistry cashregistry = new CashRegistry();
    double total = cashregistry.calculate(unitprice, quantity, discount);
    assertEquals("The test is not passed", 24, total, 0);
  }
}
