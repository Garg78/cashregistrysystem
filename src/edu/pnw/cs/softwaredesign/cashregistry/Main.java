package edu.pnw.cs.softwaredesign.cashregistry;

import java.util.Scanner;
/**
 * The main method for the CashRegistrySystem application.
 * The system shall take the following user inputs:
 * a. Unit price for an individual item.
 * b. Quantity of the item. 
 * c. A type of discount (e.g. 10% off, 20% off, etc.)
 *  @author Arushee Garg
 *  @version 1.0
 */

public class Main {
  /**
  * Main Method is the entry point to the program.
  * @param args : input arguments to the program
  */
  public static void main(String[] args) {
    String condition;
    Scanner scanner = new Scanner(System.in);
    CashRegistry cashregistry = new CashRegistry();

    do {
      cashregistry.operation(scanner);
      System.out.println("Do you want to enter another item (yes/no) ?");
      condition = scanner.next();
    } while (condition.equals("yes"));

    System.out.println("Please make the payment. Have a nice day !");
  }
}
