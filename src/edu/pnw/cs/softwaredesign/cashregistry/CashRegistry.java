package edu.pnw.cs.softwaredesign.cashregistry;

import java.util.Scanner;

/**
 *a cash register software as a command-line Java program using Object-Oriented Techniques.
 * This class contains all the methods for calculation and for user input.
 * The system shall take the following user inputs:
 * a. Unit price for an individual item.
 * b. Quantity of the item. 
 * c. A type of discount (e.g. 10% off, 20% off, etc.)
 * @author Arushee Garg
 * @version 1.0
 */
public class CashRegistry {
  private double total;

  public CashRegistry() {
    this.total = 0.0;
  }

  /** calculate method returns the total value after performing calculation on the inputs.
   * @param unitprice : user input 1
   * @param quantity : user input 2
   * @param discount : user input 3
   * @return double
   */
  public double calculate(double unitprice, int quantity,int discount) {
    total = (unitprice * quantity * (100 - discount)) / 100 + total;
    return total;
  }
  
  /** Its an method which performs the operation i.e.tak user input and calls the calculation method
   * @param scanner : Scanner
   */
  public void operation(Scanner scanner) {

    double unitprice;
    System.out.println("Unit Price ($):");
    unitprice = scanner.nextDouble();
    int quantity;
    System.out.println("Quantity:");
    quantity = scanner.nextInt();
    int discount;
    System.out.println("Discount: \n 1. 10% off \n 2. 20% off \n "
        + "3. 30% off \n 4. 40% off \n 5. 50% off");
    discount = scanner.nextInt() * 10;
    System.out.println("Total: " + calculate(unitprice,quantity,discount));
  }
}
