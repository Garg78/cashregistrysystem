Cash Registry System
Designed and implemented a cash register software as a command-line Java program using Object-Oriented Techniques.
The system shall take the following user inputs:
a. Unit price for an individual item.
b. Quantity of the item. 
c. A type of discount (e.g. 10% off, 20% off, etc.)
The system shall continue to prompt user for more input and it shall output the total price for all the items.
Example user inputs (in blue) and outputs are demonstrated below:
Unit Price ($): 
2.52
Quantity: 
7
Discount: 
1. 10% off
2. 20% off
2
Total: 14.11
Unit Price ($): 
3
Quantity: 
2
Discount: 
1. 10% off
2. 20% off
1
Total: 19.51